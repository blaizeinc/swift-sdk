//
//  BlaizeConfiguration.swift
//  Blaize
//
//  Created by Ben Kennedy on 01/04/2020.
//  Copyright © 2020 Zephr. All rights reserved.
//

import Foundation

public class BlaizeConfiguration {
    
    public static let shared = BlaizeConfiguration()

    public var tenantId: String?
    public var env: String?
    public var overridePublicBaseUrl: URL?
    public var overrideAdminBaseUrl: URL?
    public var accessKey: String?
    public var secretKey: String?

    init() {}
    
    public func getPublicBaseUrl() -> URL? {
        
        if (overridePublicBaseUrl != nil) {
            return overridePublicBaseUrl!
        }

        return getBaseUrl("cdn")
    }
    
    public func getAdminBaseUrl() -> URL? {
        
        if (overrideAdminBaseUrl != nil) {
            return overrideAdminBaseUrl!
        }
        
        return getBaseUrl("admin")
    }
    
    private func getBaseUrl(_ type: String) -> URL? {
        
        guard let tenantId = tenantId else {
            return nil
        }
        
        guard let env = env else {
            return URL(string: "https://\(tenantId).\(type).blaize.io")
        }

        return URL(string: "https://\(tenantId).\(type).\(env).blaize.io")
    }

}
