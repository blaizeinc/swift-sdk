//
//  BlaizePublic.swift
//  Blaize
//
//  Created by Ben Kennedy on 01/04/2020.
//  Copyright © 2020 Zephr. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

public class BlaizePublic {

    public static let client = BlaizePublic()

    init() {
        session = Session.default
    }

    var session: Session

    public func register(
        email: String,
        password: String,
        attributes: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (RegisterResponse) -> Void
    ) {
       
        var payload: JSON = [
            "identifiers": ["email_address": email],
            "validators": ["password": password]
        ]
        
        if attributes != nil {
            payload["attributes"].object = JSON(attributes!)
        }
        
        registerRequest(payload, completionHandler)
    }
    
    public func register(
        username: String,
        password: String,
        attributes: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (RegisterResponse) -> Void
    ) {
       
        var payload: JSON = [
            "identifiers": ["username": username],
            "validators": ["password": password]
        ]
        
        if attributes != nil {
            payload["attributes"].object = JSON(attributes!)
        }
        
        registerRequest(payload, completionHandler)
    }
    
    public func register(
        token: String,
        attributes: Dictionary<String, Any>? = nil,
        completionHandler: @escaping (RegisterResponse) -> Void
    ) {
       
        var payload: JSON = [
            "identifiers": JSON(),
            "validators": ["token_exchange": token]
        ]
        
        if attributes != nil {
            payload["attributes"].object = JSON(attributes!)
        }
        
        registerRequest(payload, completionHandler)
    }
    
    private func registerRequest(_ payload: JSON, _ completionHandler: @escaping (RegisterResponse) -> Void) {
        
        request(path: "/blaize/register", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                print(response)
                if let sessionDetails = self.getSessionDetailsFromResponse(response) {
                    completionHandler(.success(sessionDetails))
                } else {
                    completionHandler(.badRequest)
                }
            case .failure(let error):
                print(error)
                if error.isExplicitlyCancelledError {
                    print("No Tenant ID set")
                    completionHandler(.badRequest)
                    return
                }
                
                switch error.responseCode {
                case 402:
                    completionHandler(.emailDomainBlacklisted)
                case 409:
                    completionHandler(.userAlreadyExists)
                default:
                    completionHandler(.badRequest)
                }
            }
        }
    }
    
    public func login(email: String, password: String, completionHandler: @escaping (LoginResponse) -> Void) {
       
        let payload: JSON = [
            "identifiers": ["email_address": email],
            "validators": ["password": password]
        ]
        
        loginRequest(payload, completionHandler)
    }
    
    public func login(username: String, password: String, completionHandler: @escaping (LoginResponse) -> Void) {
               
        let payload: JSON = [
            "identifiers": ["username": username],
            "validators": ["password": password]
        ]

        loginRequest(payload, completionHandler)
    }
    
    private func loginRequest(_ payload: JSON, _ completionHandler: @escaping (LoginResponse) -> Void) {
        
        request(path: "/blaize/login", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                print(response)
                if let sessionDetails = self.getSessionDetailsFromResponse(response) {
                    completionHandler(.success(sessionDetails))
                } else {
                    completionHandler(.badRequest)
                }
            case .failure(let error):
                print(error)
                if error.isExplicitlyCancelledError {
                    print("No Tenant ID set")
                    completionHandler(.badRequest)
                    return
                }

                switch error.responseCode {
                case 401:
                    completionHandler(.badPassword)
                case 404:
                    completionHandler(.userNotFound)
                default:
                    completionHandler(.badRequest)
                }
            }
        }
    }
    
    public func verifyEmail(
        email: String,
        redirect: String,
        completionHandler: @escaping (BasicResponse) -> Void
    ) {

        let payload: JSON = [
            "identifiers": ["email_address": email],
            "delivery": [
                "method": "email",
                "destination": email,
                "action": "register",
                "redirect": redirect
            ]
        ]

        request(path: "/blaize/token-exchange", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                print(response)
                completionHandler(.success)
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func anonymousSession(completionHandler: @escaping (SessionResponse) -> Void) {
        
        request(path: "/blaize/anonymous-session", method: .post) { result in
            switch result {
            case .success(let response):
                print(response)
                if let sessionDetails = self.getSessionDetailsFromResponse(response) {
                    completionHandler(.success(sessionDetails))
                } else {
                    completionHandler(.failure)
                }
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func logout(sessionId: String, completionHandler: @escaping (BasicResponse) -> Void) {
        
        request(path: "/blaize/logout", method: .post, sessionId: sessionId) { result in
            switch result {
            case .success(let response):
                print(response)
                completionHandler(.success)
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func forgetMe(sessionId: String, completionHandler: @escaping (BasicResponse) -> Void) {
        
        request(path: "/blaize/forget-me", method: .post, sessionId: sessionId) { result in
            switch result {
            case .success(let response):
                print(response)
                completionHandler(.success)
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func getUserProfile(sessionId: String, completionHandler: @escaping (ProfileResponse) -> Void) {
        
        request(path: "/blaize/profile", method: .get, sessionId: sessionId) { result in
            switch result {
            case .success(let response):
                print(response)
                if let profileResult: Dictionary<String, Any> = response.body.dictionaryObject {
                    completionHandler(.success(profileResult))
                } else {
                    print("Failed to parse profileResult")
                    completionHandler(.failure)
                }
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func updateUserProfile(sessionId: String, attributes: Dictionary<String, Any>, merge: Bool = true, completionHandler: @escaping (BasicResponse) -> Void) {

        let method: HTTPMethod = merge ? .post : .put

        request(path: "/blaize/profile", method: method, sessionId: sessionId, json: JSON(attributes)) { result in
            switch result {
            case .success(let response):
                print(response)
                completionHandler(.success)
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func getExtendedProfile(sessionId: String, appId: String, completionHandler: @escaping (ProfileResponse) -> Void) {
        
        request(path: "/blaize/profile/\(appId)", method: .get, sessionId: sessionId) { result in
            switch result {
            case .success(let response):
                print(response)
                if let profileResult: Dictionary<String, Any> = response.body.dictionaryObject {
                    completionHandler(.success(profileResult))
                } else {
                    print("Failed to parse profileResult")
                    completionHandler(.failure)
                }
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func updateExtendedProfile(sessionId: String, appId: String, payload: Dictionary<String, Any>, completionHandler: @escaping (BasicResponse) -> Void) {

        request(path: "/blaize/profile/\(appId)", method: .put, sessionId: sessionId, json: JSON(payload)) { result in
            switch result {
            case .success(let response):
                print(response)
                completionHandler(.success)
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func entitlementChallenge(
        sessionId: String,
        entitlementIds: Array<String>,
        contentIdentifier: String? = nil,
        completionHandler: @escaping (ChallengeResponse) -> Void
    ) {
       
        var payload: JSON = [
            "entitlementIds": JSON(entitlementIds)
        ]
        
        if contentIdentifier != nil {
            payload["contentIdentifier"].string = contentIdentifier
        }
        
        request(path: "/blaize/authorization/challenge", method: .post, sessionId: sessionId, json: payload) { result in
            switch result {
            case .success(let response):
                print(response)
                if let challengeResult: Dictionary<String, Bool> = response.body.dictionaryObject as? Dictionary<String, Bool> {
                    completionHandler(.success(challengeResult))
                } else {
                    print("Failed to parse challengeResult")
                    completionHandler(.failure)
                }
            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    public func accessDecision(
        sessionId: String,
        path: String? = nil,
        httpMethod: HTTPMethod = .get,
        requestHeaders: Dictionary<String, String>? = nil,
        contentMetadata: Dictionary<String, Any>? = nil,
        jwt: String? = nil,
        btr: String? = nil,
        completionHandler: @escaping (DecisionResponse) -> Void
    ) {
       
        var payload: JSON = [
            "session": sessionId,
            "http_method": httpMethod.rawValue
        ]
        if path != nil {
            payload["path"].string = path
        }
        if requestHeaders != nil {
            payload["request_headers"].object = JSON(requestHeaders!)
        }
        if contentMetadata != nil {
            payload["content_metadata"].object = JSON(contentMetadata!)
        }
        if jwt != nil {
            payload["jwt"].string = jwt
        }
        if btr != nil {
            payload["btr"].string = btr
        }
        
        request(path: "/blaize/decision-engine", method: .post, json: payload) { result in
            switch result {
            case .success(let response):
                print(response)
                var headerOperations: [[String: String?]?] = []
                for (_, dict) in response.body["headerOperations"] {
                    if (dict.dictionaryObject == nil) {
                        headerOperations.append(nil)
                        continue;
                    }
                    headerOperations.append(dict.dictionaryObject as? [String: String])
                }

                completionHandler(DecisionResponse(
                    headerOperations: headerOperations,
                    status: response.body["status"].int ?? -1,
                    body: response.body["body"].string))
            case .failure(let error):
                print(error)
                completionHandler(DecisionResponse(
                    headerOperations: [],
                    status: -1,
                    body: nil))
            }
        }
    }
    
    public func startOAuthFlow(provider: AuthProvider, completionHandler: @escaping (AuthStartResponse) -> Void) {
        
        noFollowRequest(path: "/blaize/oauth/\(provider)?client_type=ios", method: .get) { result in
            switch result {
            case .success(let response):
                print(response)
                guard let oauthLocation = response.headers.first(where: { $0.name == "Location" })?.value else {
                    print("Failed to get OAuth start location")
                    return completionHandler(.failure)
                }

                guard let oauthLocationUrl = URL(string: oauthLocation) else {
                    print("Failed to parse OAuth start location")
                    return completionHandler(.failure)
                }

                completionHandler(.success(oauthLocationUrl))

            case .failure(let error):
                print(error)
                completionHandler(.failure)
            }
        }
    }
    
    private func request(
        path: String,
        method: HTTPMethod,
        sessionId: String? = nil,
        json: JSON? = nil,
        completionHandler: @escaping (Result<ResponseDetails, AFError>) -> Void
    ) {
        
        guard let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl() else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }

        guard let requestUrl = URL(string: "\(baseUrl.absoluteString)\(path)") else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }

        session.request(requestUrl,
            method: method,
            parameters: json,
            encoder: JSONParameterEncoder.default,
            headers: getRequestHeaders(sessionId: sessionId))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let json):
                    let requestResponse = ResponseDetails(
                        headers: response.response?.headers ?? [],
                        statusCode: response.response?.statusCode ?? -1,
                        body: JSON(json))
                    completionHandler(.success(requestResponse))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
            }
    }
    
    private func noFollowRequest(
        path: String,
        method: HTTPMethod,
        sessionId: String? = nil,
        completionHandler: @escaping (Result<ResponseDetails, AFError>) -> Void
    ) {
        
        guard let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl() else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }
        
        guard let requestUrl = URL(string: "\(baseUrl.absoluteString)\(path)") else {
            completionHandler(.failure(.explicitlyCancelled))
            return
        }

        session.request(requestUrl,
            method: method,
            headers: getRequestHeaders(sessionId: sessionId))
            .redirect(using: Redirector(behavior: .doNotFollow))
            .validate(statusCode: 200..<400)
            .response { response in
                switch response.result {
                case .success:
                    let requestResponse = ResponseDetails(
                        headers: response.response?.headers ?? [],
                        statusCode: response.response?.statusCode ?? -1,
                        body: JSON())
                    completionHandler(.success(requestResponse))
                case .failure(let error):
                    completionHandler(.failure(error))
                }
            }
    }
    
    private func getRequestHeaders(sessionId: String? = nil) -> HTTPHeaders {

        var headers = HTTPHeaders.default
        headers.add(.contentType("application/json"))
        headers.add(.accept("application/json"))
        
        if sessionId != nil {
            headers.add(name: "Cookie", value: "blaize_session=\(sessionId!)")
        }
        
        return headers
    }
    
    private func getBlaizeSessionIdFromResponse(_ response: ResponseDetails) -> String? {

        let blaizeSessionCookie = response.headers.first(where: { $0.name == "Set-Cookie" })?.value ?? ""
        let regex = try? NSRegularExpression(pattern: "blaize_session=([0-9a-zA-Z_\\-]+)", options: .caseInsensitive)
        if let match = regex?.firstMatch(in: blaizeSessionCookie, options: [], range: NSRange(location: 0, length: blaizeSessionCookie.utf16.count)) {
            if let range = Range(match.range(at: 1), in: blaizeSessionCookie) {
                return String(blaizeSessionCookie[range])
            }
        }
        
        return nil
    }
    
    private func getSessionDetailsFromResponse(_ response: ResponseDetails) -> SessionDetails? {
        
        guard let sessionId = getBlaizeSessionIdFromResponse(response) else {
            return nil
        }
        
        guard let trackingId = response.body["tracking_id"].string else {
            return nil
        }

        return SessionDetails(sessionId: sessionId, trackingId: trackingId)
    }

}

public enum BasicResponse {
    case success
    case failure
}

public enum LoginResponse {
    case success(SessionDetails)
    case userNotFound
    case badPassword
    case badRequest
}

public enum SessionResponse {
    case success(SessionDetails)
    case failure
}

public enum RegisterResponse {
    case success(SessionDetails)
    case userAlreadyExists
    case emailDomainBlacklisted
    case badRequest
}

public enum ProfileResponse {
    case success(Dictionary<String, Any>)
    case failure
}

public enum ChallengeResponse {
    case success(Dictionary<String, Bool>)
    case failure
}

public struct DecisionResponse {
    public let headerOperations: Array<Dictionary<String, String?>?>
    public let status: Int
    public let body: String?
}

public enum AuthStartResponse {
    case success(URL)
    case failure
}

public enum AuthProvider {
    case google
    case apple
    case facebook
    case linkedin
    case twitter
    case microsoft
}

public struct SessionDetails {
    public let sessionId: String
    public let trackingId: String
}

private struct ResponseDetails {
    let headers: HTTPHeaders
    let statusCode: Int
    let body: JSON
}
