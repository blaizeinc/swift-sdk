# Blaize Swift SDK

## Overview
Blaize uses web APIs for all its functionality. These are split into two categories: the Admin API and the Public API. All functionality of the system can be controlled by the Admin API. The Public API is used for client-side implementations and is tightly linked to a user's session.

The Blaize Swift SDK is a wrapper around the Blaize Public API, allowing client-side use in an Apple OS integration.
You can read more about the Public API here (https://support.blaize.io/public-api).

## Installation
The Blaize Swift SDK can be added as a dependency of another Swift Package or as a Swift Package Dependency in Xcode.

To use this package, add the dependency `https://bitbucket.org/blaizeinc/swift-sdk`

## Configuration
The Swift SDK requires you to configure it with your tenantId before API calls can be made.
Do this in your AppDelegate:
```swift
import UIKit
import Blaize

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        BlaizeConfiguration.shared.tenantId = "yourTenantId"
        // Override point for customization after application launch.
        return true
    }
```

Optionally, Blaize environment can be provided, too:
```swift
BlaizeConfiguration.shared.env = "staging"
```

It is possible to specify a base URL for the Blaize service, if it is non-standard (not normally needed):
```swift
BlaizeConfiguration.shared.overridePublicBaseUrl = "http://localhost:8080"
```

## Public API Usage
### Register with email
```swift
import Blaize

func register() {
    var attributes = ["first-name": "John", "last-name": "Citizen"]

    BlaizePublic.client.register(email: "johncit@blaize.io", password: "SomePassword1", attributes: attributes) { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Logged in")
        case .userAlreadyExists:
            print("A user with that email address already exists... try resetting your password")
        case .emailDomainBlacklisted:
            print("The email address you have supplied is not allowed for registration")
        case .badRequest:
            print("One of more of the mandatory fields were not provided or are incorrect")
        }
    }
}
```

### Register with username
```swift
import Blaize

func register() {
    var attributes = ["first-name": "John", "last-name": "Citizen"]

    BlaizePublic.client.register(username: "johncitizen", password: "SomePassword1", attributes: attributes) { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Logged in")
        case .userAlreadyExists:
            print("A user with that email address already exists... try resetting your password")
        case .emailDomainBlacklisted:
            print("The email address you have supplied is not allowed for registration")
        case .badRequest:
            print("One of more of the mandatory fields were not provided or are incorrect")
        }
    }
}
```

### Register with state key token
```swift
import Blaize

func register() {
    var attributes = ["first-name": "John", "last-name": "Citizen"]

    BlaizePublic.client.register(token: "1234", attributes: attributes) { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Logged in")
        case .userAlreadyExists:
            print("A user with that email address already exists... try resetting your password")
        case .emailDomainBlacklisted:
            print("The email address you have supplied is not allowed for registration")
        case .badRequest:
            print("One of more of the mandatory fields were not provided or are incorrect")
        }
    }
}
```

### Login with email
```swift
import Blaize

func login() {
    BlaizePublic.client.login(email: "johncit@blaize.io", password: "SomePassword1") { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Logged in")
        case .userNotFound:
            print("User not found")
        case .badPassword:
            print("Password was incorrect")
        case .badRequest:
            print("Something went wrong")
        }
    }
}
```

### Login with username
```swift
import Blaize

func login() {
    BlaizePublic.client.login(username: "johncitizen", password: "SomePassword1") { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Logged in")
        case .userNotFound:
            print("User not found")
        case .badPassword:
            print("Password was incorrect")
        case .badRequest:
            print("Something went wrong")
        }
    }
}
```

### Verify email
```swift
import Blaize

func verifyEmail() {
    BlaizePublic.client.verifyEmail(email: "johncit@blaize.io", redirect: "/example") { result in
        switch result {
        case .success:
            print("Email verification sent")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Create Anonymous Session
```swift
import Blaize

func anonymousSession() {
    BlaizePublic.client.anonymousSession() { result in
        switch result {
        case .success(let blaizeSessionId):
            // persist blaizeSessionId
            print("Anonymous session created")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Start OAuth Flow
```swift
import Blaize

func startOAuthFlow() {
    BlaizePublic.client.startOAuthFlow(provider: AuthProvider.google) { result in
        switch result {
        case .success(let url):
            // Start ASWebAuthenticationSession with url
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Logout
```swift
import Blaize

func logout() {
    BlaizePublic.client.logout(sessionId: "sessionId") { result in
        switch result {
        case .success:
            print("Logged out")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Forget Me
```swift
import Blaize

func forgetMe() {
    BlaizePublic.client.forgetMe(sessionId: "sessionId") { result in
        switch result {
        case .success:
            print("User deleted")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Get User Profile
```swift
import Blaize

func getUserProfile() {
    BlaizePublic.client.getUserProfile(sessionId: "sessionId") { result in
        switch result {
        case .success(let profileResult):
            // do something with profileResult
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Update User Profile
```swift
import Blaize

func updateUserProfile() {
    // Set whether to merge or overwrite attributes
    let merge = true
    let attributes = ["first-name": "Jane"]

    BlaizePublic.client.updateUserProfile(sessionId: "sessionId", attributes: attributes, merge: merge) { result in
        switch result {
        case .success:
            print("User profile updated successfully")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Get Extended Profile
```swift
import Blaize

func getExtendedProfile() {
    BlaizePublic.client.getExtendedProfile(sessionId: "sessionId", appId: "appId") { result in
        switch result {
        case .success(let extendedProfile):
            // do something with extendedProfile
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Update Extended Profile
```swift
import Blaize

func updateExtendedProfile() {
    let payload =  ["saved": ["123", "456", "789"]]
    BlaizePublic.client.updateExtendedProfile(sessionId: "sessionId", appId: "appId", payload: payload) { result in
        switch result {
        case .success:
            print("User extended profile updated successfully")
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Entitlement Challenge
```swift
import Blaize

func entitlementChallenge() {
    let entitlementIds =  ["entId1", "entId2"]
    BlaizePublic.client.entitlementChallenge(sessionId: "sessionId", entitlementIds: entitlementIds) { result in
        switch result {
        case .success(let entitlementChallenge):
            // do something with entitlementChallenge
            // if entitlementChallenge["entId1"] == true { ... }
        case .failure:
            print("Something went wrong")
        }
    }
}
```

### Access Decision
```swift
import Blaize

func accessDecision() {
    BlaizePublic.client.accessDecision(
        sessionId: "sessionId",                     //required
        path: "/",                                  //optional
        httpMethod: .get,                           //optional
        requestHeaders: ["X-Custom": "true"],       //optional
        contentMetadata: ["content_id": "1234"],    //optional
        jwt: "eyJhbGciOiJIUzI1NiIsInR5cCI6Ik..."    //optional
        btr: "17e74b9e49e66282e55d4b7ec73de9..."    //optional
    ) { decision in
        // do something with decision
        // decision.status
        // decision.headerOperations
        // decision.body
    }
}
```
