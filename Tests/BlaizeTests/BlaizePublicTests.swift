//
//  BlaizePublicTests.swift
//  Tests
//
//  Created by Ben Kennedy on 01/04/2020.
//  Copyright © 2020 Zephr. All rights reserved.
//

import XCTest
@testable import Blaize
@testable import Alamofire
@testable import Mocker
import SwiftyJSON

class BlaizePublicTest: XCTestCase {
    
    override func setUp() {

        let configuration = URLSessionConfiguration.af.default
        configuration.protocolClasses = [MockingURLProtocol.self]
        BlaizePublic.client.self.session = Session(configuration: configuration)
    }
    
    override func tearDown() {
        BlaizeConfiguration.shared.reset()
    }

    func testRegisterNoTenant() {
        let expectation = XCTestExpectation()

        BlaizePublic.client.register(email: "test@blaize.io", password: "jank123") { result in
            switch result {
            case .badRequest:
                XCTAssert(true)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testRegisterEmailSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let password = "jank123"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "cookie": "blaize_session=9cfc6b41-dd30-45ea-9ef5-a2cd60cf4a52; Expires=Fri, 2 Apr 2021 15:08:13 GMT; Path=/;",
                  "message": "Registration successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["email_address"].stringValue, email)
            XCTAssertEqual(json["validators"]["password"].stringValue, password)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.register(email: email, password: password) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testRegisterUsernameSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let username = "testzephr"
        let password = "jank123"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "cookie": "blaize_session=9cfc6b41-dd30-45ea-9ef5-a2cd60cf4a52; Expires=Fri, 2 Apr 2021 15:08:13 GMT; Path=/;",
                  "message": "Registration successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["username"].stringValue, username)
            XCTAssertEqual(json["validators"]["password"].stringValue, password)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.register(username: username, password: password) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testRegisterEmailSuccessWithAttributes() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let password = "jank123"
        let attributes = ["firstName": "Test", "lastName": "Person"]
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "cookie": "blaize_session=9cfc6b41-dd30-45ea-9ef5-a2cd60cf4a52; Expires=Fri, 2 Apr 2021 15:08:13 GMT; Path=/;",
                  "message": "Registration successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["email_address"].stringValue, email)
            XCTAssertEqual(json["validators"]["password"].stringValue, password)
            XCTAssertEqual(json["attributes"]["firstName"].stringValue, attributes["firstName"])
            XCTAssertEqual(json["attributes"]["lastName"].stringValue, attributes["lastName"])
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.register(email: email, password: password, attributes: attributes) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testRegisterTokenExchangeSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let token = "ca93aa6f-14f1-49cf-8efa-80731951711d"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "cookie": "blaize_session=9cfc6b41-dd30-45ea-9ef5-a2cd60cf4a52; Expires=Fri, 2 Apr 2021 15:08:13 GMT; Path=/;",
                  "message": "Registration successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            print(json)
            XCTAssertTrue(json["identifiers"].isEmpty)
            XCTAssertEqual(json["validators"]["token_exchange"].stringValue, token)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.register(token: token) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testRegisterConflict() {
        let expectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "testing"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 409, data: [
            .post: """
                {
                  "status": 409,
                  "message": "user[ test@blaize.io ] already exists"
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.register(email: "test@blaize.io", password: "jank123") { result in
            switch result {
            case .userAlreadyExists:
                XCTAssert(true)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testRegisterBlacklisted() {
        let expectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "testing"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/register"), dataType: .json, statusCode: 402, data: [
            .post: """
                {
                  "status": 402,
                  "message": "Please use a different email address, this domain is not supported."
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.register(email: "test@blaize.io", password: "jank123") { result in
            switch result {
            case .emailDomainBlacklisted:
                XCTAssert(true)
            default:
                XCTFail()
            }
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testLoginEmailSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let password = "jank123"
        let sessionId = "cb3a1a68-0028-47e1-9038-4097c3892b99"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/login"), dataType: .json, statusCode: 201, data: [
            .post: """
                {
                  "cookie": "blaize_session=cb3a1a68-0028-47e1-9038-4097c3892b99; Expires=Sat, 3 Apr 2021 11:46:03 GMT; Path=/;",
                  "message": "Login successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["email_address"].stringValue, email)
            XCTAssertEqual(json["validators"]["password"].stringValue, password)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.login(email: email, password: password) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testLoginUsernameSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let username = "testzephr"
        let password = "jank123"
        let sessionId = "cb3a1a68-0028-47e1-9038-4097c3892b99"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/login"), dataType: .json, statusCode: 201, data: [
            .post: """
                {
                  "cookie": "blaize_session=cb3a1a68-0028-47e1-9038-4097c3892b99; Expires=Sat, 3 Apr 2021 11:46:03 GMT; Path=/;",
                  "message": "Login successful",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["username"].stringValue, username)
            XCTAssertEqual(json["validators"]["password"].stringValue, password)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.login(username: username, password: password) { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testLoginBadPassword() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let password = "wrong-password"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/login"), dataType: .json, statusCode: 401, data: [
            .post: """
                {
                  "status": 401,
                  "message": "Login failed"
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.login(email: email, password: password) { result in
            switch result {
            case .badPassword:
                XCTAssert(true)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testLoginUserNotFound() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let password = "jank123"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/login"), dataType: .json, statusCode: 404, data: [
            .post: """
                {
                  "status": 404,
                  "message": "User not found"
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.login(email: email, password: password) { result in
            switch result {
            case .userNotFound:
                XCTAssert(true)
            default:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testAnonymousSession() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "cb3a1a68-0028-47e1-9038-4097c3892b99"
        let trackingId = "acc8f5d6-e235-4f58-b5e3-48905997060e"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=\(sessionId); Expires=Tue, 3 Aug 3019 12:34:06 GMT; Path=/;"
        ]
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/anonymous-session"), dataType: .json, statusCode: 201, data: [
            .post: """
                {
                  "message": "Anonymous session created successfully",
                  "tracking_id": "\(trackingId)"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.register()

        BlaizePublic.client.anonymousSession() { result in
            switch result {
            case .success(let sessionDetails):
                XCTAssertEqual(sessionDetails.sessionId, sessionId)
                XCTAssertEqual(sessionDetails.trackingId, trackingId)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testLogoutSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "ec2427f1-5dc4-452b-b0af-07d754b349a2"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let additionalHeaders = [
            "Set-Cookie": "blaize_session=; Expires=Thu, 01 Jan 1970 00:00:01 GMT; Path=/;"
        ]
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/logout"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "message": "Session deleted"
                }
            """.data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.onRequest = { request, postBodyArguments in
            XCTAssertEqual(request.headers.value(for: "Cookie"), "blaize_session=\(sessionId)")
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.logout(sessionId: sessionId) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testForgetMeSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "ec2427f1-5dc4-452b-b0af-07d754b349a2"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/forget-me"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "message": "User deleted successfully"
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            XCTAssertEqual(request.headers.value(for: "Cookie"), "blaize_session=\(sessionId)")
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.forgetMe(sessionId: sessionId) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testGetUserProfileSuccess() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/profile"), dataType: .json, statusCode: 200, data: [
            .get: """
                {
                  "first-name": "Ben",
                  "agree": true
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.getUserProfile(sessionId: sessionId) { result in
            switch result {
            case .success(let profile):
                XCTAssertEqual(profile["first-name"] as? String, "Ben")
                XCTAssertEqual(profile["agree"] as? Bool, true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testUpdateUserProfileMergeSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let attributes: Dictionary<String, Any> = ["firstName": "Test", "accept": true]
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/profile"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "message": "User profile updated"
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["firstName"].stringValue, attributes["firstName"] as? String)
            XCTAssertEqual(json["accept"].boolValue, attributes["accept"] as? Bool)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.updateUserProfile(sessionId: sessionId, attributes: attributes) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testUpdateUserProfileOverwriteSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let attributes: Dictionary<String, Any> = ["firstName": "Test", "accept": false]
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/profile"), dataType: .json, statusCode: 200, data: [
            .put: """
                {
                  "message": "User profile updated"
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["firstName"].stringValue, attributes["firstName"] as? String)
            XCTAssertEqual(json["accept"].boolValue, attributes["accept"] as? Bool)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.updateUserProfile(sessionId: sessionId, attributes: attributes, merge: false) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testGetExtendedProfileSuccess() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let appId = "testing"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        let mock = Mock(url: baseUrl.appendingPathComponent("/blaize/profile/\(appId)"), dataType: .json, statusCode: 200, data: [
            .get: """
                {
                  "testing": 123,
                  "another": "yes"
                }
            """.data(using: .utf8)!
        ])
        mock.register()

        BlaizePublic.client.getExtendedProfile(sessionId: sessionId, appId: appId) { result in
            switch result {
            case .success(let extendedProfile):
                XCTAssertEqual(extendedProfile["testing"] as? Int, 123)
                XCTAssertEqual(extendedProfile["another"] as? String, "yes")
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }
    
    func testUpdateExtendedProfileSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let appId = "testing"
        let payload: Dictionary<String, Any> = ["extra": "data", "array": ["of", "things"]]
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/profile/\(appId)"), dataType: .json, statusCode: 200, data: [
            .put: """
                {
                  "message": "User extended profile updated"
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["extra"].stringValue, payload["extra"] as? String)
            XCTAssertEqual(json["array"], JSON(payload["array"]!))
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.updateExtendedProfile(sessionId: sessionId, appId: appId, payload: payload) { result in
            switch result {
            case .success:
                XCTAssert(true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testEntitlementChallenge() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let ent1 = "f7f6917a-6086-478f-b982-69a78dc98259"
        let ent2 = "1a4db655-79fd-4c40-b060-0acd29e63f71"
        let entitlementIds = ["\(ent1)", "\(ent2)"]
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/authorization/challenge"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "f7f6917a-6086-478f-b982-69a78dc98259": false,
                  "1a4db655-79fd-4c40-b060-0acd29e63f71": true
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["entitlementIds"][0].stringValue, ent1)
            XCTAssertEqual(json["entitlementIds"][1].stringValue, ent2)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.entitlementChallenge(sessionId: sessionId, entitlementIds: entitlementIds) { result in
            switch result {
            case .success(let entitlementChallenge):
                XCTAssertEqual(entitlementChallenge[ent1], false)
                XCTAssertEqual(entitlementChallenge[ent2], true)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testDecisionEngineMinimal() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/decision-engine"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "status": 401,
                  "body": null,
                  "headerOperations": []
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
             
            XCTAssertEqual(json["session"].stringValue, sessionId)
            XCTAssertEqual(json["http_method"].stringValue, HTTPMethod.get.rawValue)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.accessDecision(
            sessionId: sessionId
        ) { decision in
            XCTAssertEqual(decision.status, 401)
            XCTAssertEqual(decision.body, nil)
            XCTAssertEqual(decision.headerOperations, [])
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testDecisionEngineAllOptions() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let sessionId = "c63f6111-c8a5-4f7b-ac57-f2bbe63377a0"
        let path = "/"
        let httpMethod = HTTPMethod.post
        let requestHeaders = ["X-Testing": "true"]
        let contentMetadata = ["content_id": "1234"]
        let jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
        let btr = "123-456-789"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/decision-engine"), dataType: .json, statusCode: 200, data: [
            .post: """
                {
                  "status": 200,
                  "body": "Hello there",
                  "headerOperations": [
                    null,
                    { "X-Test": "Testing" },
                    { "Location": null }
                  ]
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
             
            XCTAssertEqual(json["session"].stringValue, sessionId)
            XCTAssertEqual(json["path"].stringValue, path)
            XCTAssertEqual(json["http_method"].stringValue, httpMethod.rawValue)
            XCTAssertEqual(json["request_headers"]["X-Testing"].stringValue, requestHeaders["X-Testing"])
            XCTAssertEqual(json["content_metadata"]["content_id"].stringValue, contentMetadata["content_id"])
            XCTAssertEqual(json["jwt"].stringValue, jwt)
            XCTAssertEqual(json["btr"].stringValue, btr)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.accessDecision(
            sessionId: sessionId,
            path: path,
            httpMethod: httpMethod,
            requestHeaders: requestHeaders,
            contentMetadata: contentMetadata,
            jwt: jwt,
            btr: btr
        ) { decision in
            XCTAssertEqual(decision.status, 200)
            XCTAssertEqual(decision.body, "Hello there")
            XCTAssertEqual(decision.headerOperations[0], nil)
            XCTAssertEqual(decision.headerOperations[1]?["X-Test"], "Testing")
            XCTAssertEqual(decision.headerOperations[2]?["Location"], nil)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }
    
    func testStartOAuthFlowSuccess() {
        let finishExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let additionalHeaders = ["Location" : "https://accounts.google.com/o/oauth2/auth?client_id=1234.apps.googleusercontent.com&redirect_uri=http://test.blaize.io/blaize/oauth/google/ios/callback&response_type=code&state=123456789&scope=profile%20email"]
        let requestUrl = URL(string: "\(BlaizeConfiguration.shared.getPublicBaseUrl()!.absoluteString)/blaize/oauth/google?client_type=ios")!
        let mock = Mock(url: requestUrl, dataType: .html, statusCode: 302, data: [
            .get: "".data(using: .utf8)!
        ], additionalHeaders: additionalHeaders)
        mock.register()

        BlaizePublic.client.startOAuthFlow(provider: AuthProvider.google) { result in
            switch result {
            case .success(let url):
                XCTAssertEqual(url.absoluteString, additionalHeaders["Location"]!)
            case .failure:
                XCTFail()
            }
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation], timeout: 5.0)
    }

    func testVerifyEmailSuccess() {
        let finishExpectation = XCTestExpectation()
        let onRequestExpectation = XCTestExpectation()

        BlaizeConfiguration.shared.tenantId = "test"
        let email = "test@blaize.io"
        let redirect = "/"
        let baseUrl = BlaizeConfiguration.shared.getPublicBaseUrl()!
        var mock = Mock(url: baseUrl.appendingPathComponent("/blaize/token-exchange"), dataType: .json, statusCode: 201, data: [
            .post: """
                {
                  "message": "You have been sent an email to access this content. Check your inbox."
                }
            """.data(using: .utf8)!
        ])
        mock.onRequest = { request, postBodyArguments in
            let json = JSON(postBodyArguments!)
            XCTAssertEqual(json["identifiers"]["email_address"].stringValue, email)
            XCTAssertEqual(json["delivery"]["method"].stringValue, "email")
            XCTAssertEqual(json["delivery"]["destination"].stringValue, email)
            XCTAssertEqual(json["delivery"]["action"].stringValue, "register")
            XCTAssertEqual(json["delivery"]["redirect"].stringValue, redirect)
            onRequestExpectation.fulfill()
        }
        mock.register()

        BlaizePublic.client.verifyEmail(email: email, redirect: redirect) { result in
            XCTAssertEqual(result, .success)
            finishExpectation.fulfill()
        }

        wait(for: [finishExpectation, onRequestExpectation], timeout: 5.0)
    }

}
