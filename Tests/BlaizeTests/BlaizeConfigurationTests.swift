//
//  BlaizeConfigurationTests.swift
//  BlaizeTests
//
//  Created by Ben Kennedy on 01/04/2020.
//  Copyright © 2020 Zephr. All rights reserved.
//

import XCTest
@testable import Blaize

class BlaizeConfigurationTest: XCTestCase {

    override func tearDown() {
        BlaizeConfiguration.shared.reset()
    }

    func testInit() {
        let sharedConfig = BlaizeConfiguration.shared;
        XCTAssertNil(sharedConfig.tenantId)
        XCTAssertNil(sharedConfig.env)
        XCTAssertNil(sharedConfig.overridePublicBaseUrl)
        XCTAssertNil(sharedConfig.overrideAdminBaseUrl)
        XCTAssertNil(sharedConfig.accessKey)
        XCTAssertNil(sharedConfig.accessKey)
    }

    func testSharedConfig() {
        BlaizeConfiguration.shared.tenantId = "testTenant"
        XCTAssertEqual(BlaizeConfiguration.shared.tenantId, "testTenant")
        
        BlaizeConfiguration.shared.env = "staging"
        XCTAssertEqual(BlaizeConfiguration.shared.env, "staging")
        
        BlaizeConfiguration.shared.overridePublicBaseUrl = URL(string: "http://localhost:8080")
        XCTAssertEqual(BlaizeConfiguration.shared.overridePublicBaseUrl, URL(string: "http://localhost:8080"))
        
        BlaizeConfiguration.shared.overrideAdminBaseUrl = URL(string: "http://localhost:8081")
        XCTAssertEqual(BlaizeConfiguration.shared.overrideAdminBaseUrl, URL(string: "http://localhost:8081"))
        
        BlaizeConfiguration.shared.accessKey = "1234"
        XCTAssertEqual(BlaizeConfiguration.shared.accessKey, "1234")
        
        BlaizeConfiguration.shared.secretKey = "1234"
        XCTAssertEqual(BlaizeConfiguration.shared.secretKey, "1234")
    }

    func testGetPublicUrlNil() {
        XCTAssertEqual(BlaizeConfiguration.shared.getPublicBaseUrl(), nil)
    }

    func testGetPublicUrlTenant() {
        BlaizeConfiguration.shared.tenantId = "test-tenant"
        XCTAssertEqual(BlaizeConfiguration.shared.getPublicBaseUrl(), URL(string: "https://test-tenant.cdn.blaize.io"))
    }

    func testGetPublicUrlOverride() {
        BlaizeConfiguration.shared.overridePublicBaseUrl = URL(string: "http://localhost:8080")
        XCTAssertEqual(BlaizeConfiguration.shared.getPublicBaseUrl(), URL(string: "http://localhost:8080"))
    }

    func testGetAdminUrlNil() {
        XCTAssertEqual(BlaizeConfiguration.shared.getAdminBaseUrl(), nil)
    }

    func testGetAdminUrlTenant() {
        BlaizeConfiguration.shared.tenantId = "test-tenant"
        XCTAssertEqual(BlaizeConfiguration.shared.getAdminBaseUrl(), URL(string: "https://test-tenant.admin.blaize.io"))
    }

    func testGetAdminUrlTenantEnv() {
        BlaizeConfiguration.shared.tenantId = "test-tenant"
        BlaizeConfiguration.shared.env = "staging"
        XCTAssertEqual(BlaizeConfiguration.shared.getAdminBaseUrl(), URL(string: "https://test-tenant.admin.staging.blaize.io"))
    }

    func testGetAdminUrlOverride() {
        BlaizeConfiguration.shared.overrideAdminBaseUrl = URL(string: "http://localhost:8081")
        XCTAssertEqual(BlaizeConfiguration.shared.getAdminBaseUrl(), URL(string: "http://localhost:8081"))
    }

}

extension BlaizeConfiguration {
    func reset() {
        tenantId = nil
        env = nil
        overridePublicBaseUrl = nil
        overrideAdminBaseUrl = nil
        accessKey = nil
        secretKey = nil
    }
}
